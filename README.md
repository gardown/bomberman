# README #

Programming Test – Bomberman (Unreal)
Steps
1. Create a public Git repository on Bitbucket, GitHub, or any other Server
2. Push a first commit including only the README.md and .gitignore file
3. Follow the tasks and commit in meaningful steps, so we can follow your changes
4. At the end, take some time to explain your next steps if you would continue working on the project. Add the time you spent on the test to the README.md file and send us the link to your repository.
Please highlight any content that is not created by yourself (separate folder, obviously commented, separated code section) and further specify the source in the README.md file.
Task
Please send us your result within one week. You should not spend more than 15 hours on the test.
Your goal is to create a quick and dirty “programmer-art” version of the old Dyna Blaster/Bomberman game in 3D. Please use a mix of C++ and Blueprints.
Thank you for taking your time and participating in the test!
Features
• Static top-down view on the level (Bonus: camera that pans in/out depending on distance between players in the level)
• One pre-defined map (Bonus: procedural generated maps)
• “Couch Co-Op”: Two players play on one machine using different keys to control two characters
	o Make the characters distinguishable somehow (color, nameplate, …)
	o Optimally with both players using the keyboard, no gamepad
• Implementation of different pickups
	o Longer bomb blasts
	o More bombs
	o Faster run speed
	o Remote-controlled bombs (timed power-up, around 10 seconds)
• Bomb placing by the player
	o Player is starting with only one bomb that can be active at a time
	o Placing a bomb subtracts one from the count, when the bomb explodes the count goes up again
	o Amount is upgradable with pickups
	o Once the remote detonator has been picked up only one bomb can be active until the power-up runs out
• Player death when standing in bomb blast
• Bomb blasts
	o Should not be spherical but linear in the four main directions
	o Are stopped by walls
	o Trigger other bombs
• Differentiation between destructible and indestructible walls, destructible walls can spawn random pickups (~30% chance to spawn something) upon destruction
• Win conditions:
	o Show win screen when only one player is alive
	o Show a map timer, that counts down and ends the round
	o Show draw when the last players die in the same bomb blast (or chained bombs) or multiple players are alive when the timer runs out
	o After round end, freeze game in its current state
• Reset option on end screen
	o Starts another round
	o Previous score should be saved
• Bonus: AI enemies that behave like a player

If you need any reference, you can find some gameplay here: 
https://www.youtube.com/watch?v=DMNxOmNzfb0 
 



# Tasks needed #

1. Start BomberMan repository with readme ( this file ) and gitignore empty for now.  	**** 1-11-2017
2. Setup Unreal engine 																	**** 5-11-2017
3. Setup Git tools for repository 														**** 5-11-2017
4. Write game Rules 
5. Setup scene , Camera & lighting 														**** 5-11-2017  
6. Create side walls 																	**** 5-11-2017
7. Create destructible blocks 															**** 7-11-2017
8. Create indestructible blocks 														**** 6-11-2017 
9. Make a level map 																	**** 6-11-2017  
10. Create players char  																**** 6-11-2017	
11. Make each player move and detect walls 												**** 6-11-2017 
12. Create bomb																			**** 8-11-2017
13. Bomb placment																		**** 8-11-2017
14. Explode bomb and destroyed objects in way											**** 8-11-2017
15. Create pickups and player picking them up.
16. Do Hud
17. Win conditions




commit 5-11-2017 	
2nd part of start This includes initial setup of Floor, side walls, camera, a player and its control player can move in 4 directions. Problems to solve 1. When game runs we have 2 cameras 2. Player is far too small Time approx 3hrs Including setinmg up git

Commit 6-11-2017
Added Spawn 2 playes and each to use there own keys.
Need to setup up start postions for each player.
Time taken 4hrs as needed to learn a bit more about unity.
And find the best way to do the keys for each player as unity only attaches keyBoard to one player.

Commit 6-11-2017
Player1 right changed values as wrong way round
Added NoneDestoryWall blocks in map
Time Taken 2hrs 
Problems TODO: 
Need to sort out lighting.
Player when spawned does not hit floor.
Play Area slight wrong size


 Commit 7-11-2017
 added wall textures also redone map as walls was going though the floor and messed up lighting
 Create destructible blocks in random postions
Fixed +++++TODO lighting needs work fixed
Fixed +++++TODO Play Area slight wrong size
Fixed ++++TODO Player when spawned does not hit floor.
Time Taken 1 day most of the time was learning unrealy and also some time taken using editor to redo map and add textures
Some time lost due to losing work in the editor. I ran the editor from VS did a lot of changes to the wall blocks and then stopped the debugging with out saving.
Now I know it wont happen again and also need to sort out auto save.

Commit 8-11-2017
12. Create bomb																			**** 8-11-2017
13. Bomb placment																		**** 8-11-2017
14. Explode bomb and destroyed objects in way											**** 8-11-2017
Player is now red or blue
TODO Set random position of player in a free space
TODO as the game now has 2 gameStarts we will need to change its coordinates 
TODO compile does not like using FString need to find out way and what to do
TODO add anim for bomb explosion
TODO if Destructible wall is hit random spawn a pickup
TODO if hit player kill it
TODO add anim for wall explosion
TODO need to change code so that each player has it own count of max bombs

Blue player is setup I=up K=down J=left L=right insert=Place bomb
Red Player is setup W=up S=down A=left D=right Space=Place bomb
Bomb Explode Time is 10 seconds
Players can place 10 bombs between them this is a bug see TODO above
Time Taken 1 day now starting to get on better in the unreal engine 

Commint  9-11-2017
Added rand start player postion
TODO need to test that player is not trapped
Time taken 30 mins


