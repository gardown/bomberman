/*************************************************************************************************

Description:
Class to Spawn Destructible Walls

Notes:
	numberOfDestructibleWalls is the amount to walls to spawn change changeable in the editor


Copyright (c) The BitMap Brothers. All rights reserved.

*************************************************************************************************/


#include "DestructibleSpawner.h"
#include "DestructibleWall.h"
#include "Math/UnrealMathUtility.h"
#include "UObject/ConstructorHelpers.h"



const int32 MaxY = 6;
const int32 MinY = -6;
const int32 MaxX = 5;
const int32 MinX = -5;


//*************************************************************************************************
//## void ADestructibleSpawner()
//
//  Description:
//    Constructor
//
//*************************************************************************************************

ADestructibleSpawner::ADestructibleSpawner()
	:numberOfDestructibleWalls(20)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	// we need to get our BluePrint for our DestructibleWall to use in spawn
	static ConstructorHelpers::FObjectFinder<UBlueprint> blueprint(TEXT("Blueprint'/Game/BluePrints/BP_DestructibleWall.BP_DestructibleWall'"));
	checkf(blueprint.Object != NULL, TEXT("Cant find BluePrint ERROR"));
	blueprintObj = (UClass*)blueprint.Object->GeneratedClass;
}

//*************************************************************************************************
//## void BeginPlay()
//
//  Description:
//    Called when the game starts or when spawned.
//
//  Parameters:
//    None
//
//  Returns:
//    None
//
//*************************************************************************************************

void ADestructibleSpawner::BeginPlay()
{
	Super::BeginPlay();
	int32 count = 0;
	// spawn the number of walls required
	while (count<numberOfDestructibleWalls)
	{
		// get a random position to place wall
		int32 x = FMath::RandRange(MinX, MaxX);
		int32 y = FMath::RandRange(MinY, MaxY);
		FVector location(x*300.0f, y*300.0f, 351.0f);
		FRotator rotation(0.0f, 0.0f, 0.0f );
		FActorSpawnParameters spawnInfo;
		spawnInfo.Owner=this;
		spawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;
		// As we dont wont it to ovelay any other object tell unity not to spawn if a collision 
		//TODO we may have to change the random ness latter to add other rules
		ADestructibleWall* newWall=GetWorld()->SpawnActor<ADestructibleWall>(blueprintObj,location, rotation, spawnInfo);
		// If null than we have a Collision 
		if( newWall!=NULL)
		{ 
			//newWall->SetActorScale3D(scale);
			count++;
		}
	}
}

