/*************************************************************************************************

  Description: 
   This is a bombHandler for bombs

    Copyright (c) The Bitmap Brothers. All rights reserved.

*************************************************************************************************/

#pragma once

#include "CoreMinimal.h"
#include "BombGameMode.h"
#include "GameFramework/Actor.h"
#include "BombHandler.generated.h"



UCLASS()
class BOMB_API ABombHandler : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ABombHandler();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
public:

	void PlantBomb(FVector location , int32 playerIndex );

	// Called every frame
	virtual void Tick(float DeltaTime) override;
private:
	void DoExplode4Direction( class ALiveBomb* bomb );
	void TestForBombADirection(FVector beginLocation, FVector endLocation, ALiveBomb* bomb );


	 

//Data 
protected:

	// the number of max bombs allowed can increase by pickups
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bombs", Meta = (BlueprintProtect = "true"))
	int32	maxBombs;

	// the number of max bombs allowed can increase by pickups
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bombs", Meta = (BlueprintProtect = "true"))
	float	bombTimeToExplode;

private:
	TArray<class ALiveBomb*> bombArray;
	TSubclassOf<class ALiveBomb> blueprintObj;
};
