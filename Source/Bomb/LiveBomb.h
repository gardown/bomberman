/*************************************************************************************************

Description:
The Bomb

Copyright (c) The Bitmap Brothers. All rights reserved.

*************************************************************************************************/
#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "LiveBomb.generated.h"

UCLASS()
class BOMB_API ALiveBomb : public AStaticMeshActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALiveBomb();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	FVector GetBombSpawnlocation() const { return bombSpawnlocation; }
	void	SetBombSpawnlocation(FVector val) { bombSpawnlocation = val; }
	double	GetBombTimeToExplode() const { return bombTimeToExplde; }
	void	SetBombTimeToExplode(double val) { bombTimeToExplde = val; }
	int32	GetPlayerIndex() const { return playerIndex; }
	void	SetPlayerIndex(int32 val) { playerIndex = val; }

//data
private:
	FVector bombSpawnlocation;
	double bombTimeToExplde;
	int32 playerIndex;
public:
	
};
