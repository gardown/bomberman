/*************************************************************************************************

Description:
This is a bombHandler for bombs

Copyright (c) The Bitmap Brothers. All rights reserved.

*************************************************************************************************/


#include "BombHandler.h"
#include "LiveBomb.h"
#include "DestructibleWall.h"
#include "UObject/ConstructorHelpers.h"
#include "Misc/App.h"

// These defaults can be over written in each level   
const int32 DEFAULT_MAX_BOMBS = 10;
const float DEFAULT_BOMB_TIME_TO_EXPLODE = 10.0f; // in seconds
const float DEFULT_BLOCK_SIZE = 600.f;


//*************************************************************************************************
//## void ABombHandler()
//
//  Description:
//    Constructor
//
//*************************************************************************************************

ABombHandler::ABombHandler()
	: maxBombs(DEFAULT_MAX_BOMBS)
	, bombTimeToExplode(DEFAULT_BOMB_TIME_TO_EXPLODE)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryActorTick.bCanEverTick = true;
	// we need to get our BluePrint for our bomb to use in spawn
	static ConstructorHelpers::FObjectFinder<UBlueprint> blueprint(TEXT("Blueprint'/Game/BluePrints/BP_LiveBomb.BP_LiveBomb'"));
	checkf(blueprint.Object != NULL, TEXT("Cant find BluePrint ERROR"));
	blueprintObj = (UClass*)blueprint.Object->GeneratedClass;
	bombArray.Empty();
	if (bombArray.Num() > 0)
	{
		UE_LOG(LogTemp, Display, TEXT("%s"), TEXT("Help"));
	}
}



//*************************************************************************************************
//## void BeginPlay()
//
//  Description:
//    Called when the game starts or when spawned.
//
//  Parameters:
//    None
//
//  Returns:
//    None
//
//*************************************************************************************************

void ABombHandler::BeginPlay()
{
	Super::BeginPlay();
	bombArray.Empty();
	// ...
	
}



//*************************************************************************************************
//## void Tick(float DeltaTime)
//
//  Description:
//    Does the bomb handler tick
//	  This test each bomb placed in map to see if its time has run out
//	  If bomb explodes then also do a 2 block chain reaction with other bombs in the area in 4 directions
//    If hit a any wall in a 4 way direction stop detect of the explosion
//	  *** TODO
//	  if Destructible wall is hit random spawn a pickup
//	  *** TODO
//	  if hit player kill it
//
//  Parameters:
//    DeltaTime : number of second passed for frame
//
//  Returns:
//    None
//
//	Notes:
//		*** TODO add anim for bomb explosion 
//		*** TODO if Destructible wall is hit random spawn a pickup
//		*** TODO if hit player kill it
//
//*************************************************************************************************

void ABombHandler::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	double timeNow = FApp::GetCurrentTime();
	bool restart=true;
	while (restart)
	{
		restart=false;
		for(int32 index=0;index<bombArray.Num();index++)
		{
			ALiveBomb* bomb= bombArray[index];
			// See if we need to explode the bomb because it timed out
			if (bomb->GetBombTimeToExplode() <= timeNow )
			{
				//do bomb with 4 way explosion
				DoExplode4Direction( bomb );
				FVector location=bomb->GetActorLocation();
				// destroy this bomb and remove from array we need to break this loop and restart as the array it changed
				// this will also have the effect to explode any other bomb in path
				//TODO add anim for bomb explosion 
				bomb->Destroy();
				bombArray.RemoveAt(index);
				restart=true;
				break;
			}
		}
	}
}


//*************************************************************************************************
//## void DoExplode4Direction(ALiveBomb* bomb )
//
//  Description:
//    ?
//
//  Parameters:
//    bomb : ALiveBomb object to explode
//
//  Returns:
//    None
//
//
//*************************************************************************************************

void ABombHandler::DoExplode4Direction(ALiveBomb* bomb )
{

	FVector beginLocation=bomb->GetActorLocation();
	FVector endLocation=beginLocation;
	// get all bombs 2 blocks in y+
	endLocation.Y+= DEFULT_BLOCK_SIZE;
	TestForBombADirection( beginLocation , endLocation , bomb );
	endLocation = beginLocation;
	// get all bombs 2 blocks in y-
	endLocation.Y -= DEFULT_BLOCK_SIZE;
	TestForBombADirection(beginLocation, endLocation, bomb);
	endLocation = beginLocation;
	// get all bombs 2 blocks in x+
	endLocation.X +=  DEFULT_BLOCK_SIZE;
	TestForBombADirection(beginLocation, endLocation, bomb);
	endLocation = beginLocation;
	// get all bombs 2 blocks in x-
	endLocation.X -=  DEFULT_BLOCK_SIZE;
	TestForBombADirection(beginLocation, endLocation, bomb);
}




//*************************************************************************************************
//## void TestForBombADirection(FVector beginLocation, FVector endLocation, ALiveBomb* bomb)
//
//  Description:
//    ?
//
//  Parameters:
//    beginLocation : Location of the Bomb
//    endLocation   : Location of the Bomb + the lenght to test
//    bomb          : ?
//
//  Returns:
//    None
//
//  Notes:
//		***TODO add anim for wall explosion
//		*** TODO if Destructible wall is hit random spawn a pickup
//		*** TODO if hit player kill it 
//
//*************************************************************************************************
void ABombHandler::TestForBombADirection(FVector beginLocation, FVector endLocation, ALiveBomb* bomb)
{
	// test to find other Bombs
	TArray<FHitResult> hitResults;
	if ( GetWorld()->LineTraceMultiByChannel(hitResults,beginLocation,endLocation, ECC_Destructible ) )
	{
		for (int32 index = 0 ; index < hitResults.Num() ; index++ )
		{

			ADestructibleWall* foundWall = Cast<ADestructibleWall>(hitResults[index].GetActor());
			if (foundWall)
			{
				//if found wall destroy it
				foundWall->Destroy();
				//TODO add anim for wall explosion 
				// as its a wall its a blocker so return now
				return;
			}
			ALiveBomb*  foundBomb = Cast<ALiveBomb>(hitResults[index].GetActor());
			// make sure its a live bomb and its not the same as passed
			if(foundBomb !=NULL  && foundBomb != bomb )
			{
				// find found bomb in the bomb array so we can acted on it
				for (int32 ix = 0; ix<bombArray.Num(); ix++)
				{
 					if (foundBomb == bombArray[ix] )
					{
						// set bomb to explode and set to the call bomb player so he can claim any points
						bombArray[ix]->SetBombTimeToExplode( 0 );
						bombArray[ix]->SetPlayerIndex(bomb->GetPlayerIndex());
					}
				} 
			}
		}
	}
}



//*************************************************************************************************
//## void PlantBomb( FVector location , int32 playIndex)
//
//  Description:
//    ?
//
//  Parameters:
//    location  : Location of the player that wonts to place bomb
//    playIndex : Play index need to keep score if needed 
//
//  Returns:
//    None
//
//  Notes:
//		TODO need to change code so that each player has it own count of max bombs
//
//*************************************************************************************************
void ABombHandler::PlantBomb( FVector location , int32 playIndex)
{
	// get player location then snap to middle of the square on x and y only.
	FVector v= location;
	float	fx = (v.X / (DEFULT_BLOCK_SIZE/2.0f)) + 0.5f;
	float	fy = (v.Y / (DEFULT_BLOCK_SIZE/2.0f)) + 0.5f;
	int32	ix = floor(fx);
	int32	iy = floor(fy);
	v.X = ix;
	v.X *=(DEFULT_BLOCK_SIZE/2.0f);
	v.Y = iy;
	v.Y *= (DEFULT_BLOCK_SIZE/2.0f);
	// make sure we can place a bomb
	if (bombArray.Num() < maxBombs)
	{
		// We need to check that not planting a bomb in the same place
		for (int32 index = 0; index<bombArray.Num(); index++)
		{
			// if Spawn location of 2 bombs are the same dont plant a new one
			if (bombArray[index]->GetBombSpawnlocation() == v )
			{
				return;
			}
		}
		FRotator rotation(0.0f, 0.0f, 0.0f);
		FActorSpawnParameters spawnInfo;
		spawnInfo.Owner = this;
		//spawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;
		ALiveBomb* bomb = GetWorld()->SpawnActor<ALiveBomb>(blueprintObj, v, rotation, spawnInfo);
		if (bomb != NULL)
		{
			bombArray.Add(bomb);
			bomb->SetBombSpawnlocation(v);
			double expTime= FApp::GetCurrentTime();
			expTime+=(double)bombTimeToExplode;
			bomb->SetBombTimeToExplode(expTime);	
			bomb->SetPlayerIndex(playIndex);
		}
	}
}

