/*************************************************************************************************

Description:
This is my main Character mode Class for BombMan

Notes:
Was made from a Unreal new c++ project ( Topdown ).
Define CUTOUT is used to remove Camera following code as my need it latter.


Copyright (c) The BitMap Brothers. All rights reserved.

*************************************************************************************************/

#include "BombCharacter.h"
#include "BombHandler.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/GameplayStatics.h"



const int32 MaxY = 6;
const int32 MinY = -6;
const int32 MaxX = 5;
const int32 MinX = -5;


 
//*************************************************************************************************
//## void ABombCharacter()
//
//  Description:
//    Constructor
//
//  Parameters:
//    None
//
//  Returns:
//    None
//
//  Notes:
//    Define CUTOUT is used to remove Camera following code as my need it latter.
//
//*************************************************************************************************

ABombCharacter::ABombCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	GetCharacterMovement()->bUseFlatBaseForFloorChecks = true;

	//bombHandler = CreateDefaultSubobject<ABombHandler>(TEXT("bombHandler"));


#ifndef CUTOUT // We dont need the below as we wont a top down view
	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

#endif //#ifndef CUTOUT

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}



//*************************************************************************************************
//## void Tick(float DeltaSeconds)
//
//  Description:
//	Called once a frame while game is running    
//
//  Parameters:
//    DeltaSeconds : number of second passed for frame
//
//  Returns:
//    None
//
//  Notes:
//    Define CUTOUT is used to remove Camera following code as my need it latter.
//
//*************************************************************************************************

void ABombCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
#ifndef CUTOUT   //We dont wont any mouse clicks 
	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
#endif // #ifndef CUTOUT
}


//*************************************************************************************************
//## void BeginPlay()
//
//  Description:
//    Called when the game starts or when spawned.
//	  Change player color blue or red
//    *** TODO
//	  Set random position of player in a free space
//  Parameters:
//    None
//
//  Returns:
//    None
//
//  Notes:
//    *** TODO Set random position of player in a free space
//
//*************************************************************************************************
void ABombCharacter::BeginPlay()
{
	
	Super::BeginPlay();
	UMaterialInstanceDynamic*  dynamicMaterial = UMaterialInstanceDynamic::Create(GetMesh()->GetMaterial(0), this);
	ABombCharacter* myC = Cast<ABombCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	FLinearColor color(0.0f, 0.0f, 1.0f, 0.0f);
	// if player 0 player will be Red
	if( this== myC)
	{
		color.R=1.0f;
		color.B=0.0f;
	}
	dynamicMaterial->SetVectorParameterValue("BodyColor", color);
	GetMesh()->SetMaterial(0, dynamicMaterial);
	RandPlayerPostion();
}



//*************************************************************************************************
//## void RandPlayerPostion()
//
//  Description:
//    set the rand player location snap to blocks
//
//  Parameters:
//    None
//
//  Returns:
//    None
//
//  Notes:
//		TODO need to test that player is not trapped
//
//*************************************************************************************************

void ABombCharacter::RandPlayerPostion()
{
	bool notFound=true;
	// keep trying until a space is found
	while (notFound)
	{
		// get a random position to place wall
		int32 x = FMath::RandRange(MinX, MaxX);
		int32 y = FMath::RandRange(MinY, MaxY);
		float fx = x*300.0f;
		float fy = y*300.f;
		FVector beginLocation(fx, fy, 2000.0f);
		FVector endLocation(fx, fy, 61.0f);
		// test to find other Bombs
		FHitResult hitResults;
		if ( !  GetWorld()->LineTraceSingleByChannel(hitResults, beginLocation, endLocation, ECC_Visibility))
		{
			FVector v= GetActorLocation();
			v.X = fx;
			v.Y = fy;
			SetActorLocation(v);
			notFound=false;
		}
	}
}


//*************************************************************************************************
//## void MoveForward(float Value)
//
//  Description:
//    Move Character Forward or Back based on Value passed 
//
//  Parameters:
//    Value : is the amount of movement needed to move Character 
//
//  Returns:
//    None
//
//  Notes:
//		Looks like this is called every frame so if no input the Value will be zero
//    
//*************************************************************************************************

void ABombCharacter::MoveForward(float Value)
{
	//only do movement if we have a controller and an input
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

//*************************************************************************************************
//## void MoveRight(float Value)
//
//  Description:
//    Move Character Right or Left based on Value passed 
//
//  Parameters:
//    Value : is the amount of movement needed to move Character 
//
//  Returns:
//    None
//
//  Notes:
//		Looks like this is called every frame so if no input the Value will be zero
//    
//*************************************************************************************************
void ABombCharacter::MoveRight(float Value)
{
	//only do movement if we have a controller and an input
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

