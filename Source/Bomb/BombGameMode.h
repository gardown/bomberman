/*************************************************************************************************

Description:
This is my main Game mode Class for BombMan

Copyright (c) The BitMap Brothers. All rights reserved.

*************************************************************************************************/


#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BombGameMode.generated.h"

UCLASS(minimalapi)
class ABombGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	

	/** Called for forwards/backward input */
	void MoveForwardP0(float Value);

	/** Called for side to side input */
	void MoveRightP0(float Value);

	/** Called for forwards/backward input */
	void MoveForwardP1(float Value);

	/** Called for side to side input */
	void MoveRightP1(float Value);

	// Player has asked to Plant a Bomb 
	void PlantBombP0();

	// Player has asked to Plant a Bomb 
	void PlantBombP1();

public:
	// constuctor 
	ABombGameMode();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


private:
	// Spawn our Characters
	void	SpawnCharacters();

	void	PlantBomb(int32 playerIndex, class ABombCharacter* myC);
	
	// do key bindings 
	void DoBind();

// Data

private:
	// The number of players we wont in game.
	int32 numberOfPlayers;

};



