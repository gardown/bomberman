/*************************************************************************************************

Description:
	This is my main Character mode Class for BombMan

Notes:
	Was made from a Unreal new c++ project ( Topdown ).
	Define CUTOUT is used to remove Camera following code as my need it latter.


Copyright (c) The BitMap Brothers. All rights reserved.

*************************************************************************************************/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BombCharacter.generated.h"

// cut code out of template  
#define  CUTOUT 


UCLASS(Blueprintable)
class ABombCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ABombCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

#ifndef CUTOUT // We dont need the below as we wont a top down view
	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }
#endif //#ifndef CUTOUT

	// Called when the game starts
	virtual void BeginPlay() override;
	// Move Character Forward or Back based on Value passed 
	void MoveForward(float Value);
	// Move Character Right or Left based on Value passed
	void MoveRight(float Value);



private:

	void	RandPlayerPostion();
#if 0
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
 	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
 	class UDecalComponent* CursorToWorld;
#endif //#if cutout

};

