/*************************************************************************************************

Description:
Class to Spawn Destructible Walls

Notes:
numberOfDestructibleWalls is the amount to walls to spawn changeable in the editor


Copyright (c) The BitMap Brothers. All rights reserved.

*************************************************************************************************/
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DestructibleSpawner.generated.h"

UCLASS()
class BOMB_API ADestructibleSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADestructibleSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

//DATA
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Walls", Meta = (BlueprintProtect = "true"))
	int32 numberOfDestructibleWalls;	

private:
	TSubclassOf<class ADestructibleWall> blueprintObj;
	
	float	bombTimeToExplde;

};
