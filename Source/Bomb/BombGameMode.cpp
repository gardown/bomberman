/*************************************************************************************************

  Description: 
    This is my main Game mode Class for BombMan

    Copyright (c) The BitMap Brothers. All rights reserved.

*************************************************************************************************/


#include "BombGameMode.h"
#include "BombHandler.h"
#include "BombCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "EngineUtils.h"



//*************************************************************************************************
//## void ABombGameMode()
//
//  Description:
//    Constructor
//
//*************************************************************************************************

ABombGameMode::ABombGameMode()
	: numberOfPlayers(2)
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}




//*************************************************************************************************
//## void BeginPlay()
//
//  Description:
//    Called when the game starts or when spawned.
//
//  Parameters:
//    None
//
//  Returns:
//    None
//
//*************************************************************************************************

void ABombGameMode::BeginPlay()
{
	Super::BeginPlay();
	SpawnCharacters();
	DoBind();
}


//*************************************************************************************************
//## void SpawnCharacters()
//
//  Description:
//    Spawn our Characters
//
//  Parameters:
//    None
//
//  Returns:
//    None
// 
//
//*************************************************************************************************

void ABombGameMode::SpawnCharacters()
{
	if (numberOfPlayers)
	{
		UWorld* currentWorld = GetWorld();
		UGameInstance* gameInstance = currentWorld->GetGameInstance();
		UGameViewportClient* gameViewport = currentWorld->GetGameViewport();
		// Remove split screen
		gameViewport->SetDisableSplitscreenOverride(true);

		for (int32 playerNumber = 0;  playerNumber< numberOfPlayers; playerNumber++)
		{
			ULocalPlayer* localPlayer;
			// If playerNumber is zero we dont need to spawn because its already done
			if (playerNumber == 0)
			{
				localPlayer = gameInstance->GetFirstGamePlayer();

			}
			else
			{
				// We need to spawn a new Character
				//TODO as the game now has 2 gameStarts we will need to change its coordinates 
				FString outError;
				localPlayer = gameInstance->CreateLocalPlayer(playerNumber, outError, true);
				//TODO compile does not like using FString need to find out way and what to do 
				//checkf(localPlayer != NULL, TEXT("CreateLocalPlayer ERROR %s"), outError);
				checkf(localPlayer != NULL, TEXT("CreateLocalPlayer ERROR"));
			}
			RestartPlayer(localPlayer->PlayerController);
		}

	}
}


//*************************************************************************************************
//## void DoBind()
//
//  Description:
//    // do key bindings
//
//  Parameters:
//    None
//
//  Returns:
//    None
//
//  Notes:
//	  As a keyboard can only be used for one player and we need to have 2 players on a keyboard.
//	  Make bindings on player 0 for both players using 2 routines for each type of action.
//	  And on the actions filter that action and send to the right player controller class 
//   	  
//*************************************************************************************************

void ABombGameMode::DoBind()
{
	ABombCharacter* myC = Cast<ABombCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	UInputComponent* comp = myC->InputComponent;
	if (comp)
	{
		comp->BindAxis("MoveForwardP0", this, &ABombGameMode::MoveForwardP0);
		comp->BindAxis("MoveRightp0", this, &ABombGameMode::MoveRightP0);
		comp->BindAxis("MoveForwardP1", this, &ABombGameMode::MoveForwardP1);
		comp->BindAxis("MoveRightp1", this, &ABombGameMode::MoveRightP1);
		comp->BindAction("PlaceBombP0", IE_Pressed, this, &ABombGameMode::PlantBombP0);
		comp->BindAction("PlaceBombP1", IE_Pressed, this, &ABombGameMode::PlantBombP1);
	}
}

//*************************************************************************************************
//## void MoveForwardP0(float Value)
//
//  Description:
//    Move Player 0 Forward or Back based on Value passed 
//
//  Parameters:
//    Value : is the amount of movement needed to move Character
//
//  Returns:
//    None
//*************************************************************************************************

void ABombGameMode::MoveForwardP0(float Value)
{
	// Get ABatteryCollectorCharacter for player 0
	ABombCharacter* myC = Cast<ABombCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (myC != NULL)
	{
		myC->MoveForward(Value);
	}
}

//*************************************************************************************************
//## void MoveRightP0(float Value)
//
//  Description:
//    Move Player 0 Right or Left based on Value passed 
//
//  Parameters:
//    Value : is the amount of movement needed to move Character
//
//  Returns:
//    None
//*************************************************************************************************

void ABombGameMode::MoveRightP0(float Value)
{
	// Get ABatteryCollectorCharacter for player 0
	ABombCharacter* myC = Cast<ABombCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (myC != NULL)
	{
		myC->MoveRight(Value);
	}
}


//*************************************************************************************************
//## void MoveForwardP1(float Value)
//
//  Description:
//    Move Player 1 Forward or Back based on Value passed 
//
//  Parameters:
//    Value : is the amount of movement needed to move Character
//
//  Returns:
//    None
//*************************************************************************************************
void ABombGameMode::MoveForwardP1(float Value)
{
	// Get ABatteryCollectorCharacter for player 1
	ABombCharacter* myC = Cast<ABombCharacter>(UGameplayStatics::GetPlayerPawn(this, 1));
	if (myC != NULL)
	{
		myC->MoveForward(Value);
	}
}


//*************************************************************************************************
//## void MoveRightP1(float Value)
//
//  Description:
//    Move Player 1 Right or Left based on Value passed 
//
//  Parameters:
//    Value : is the amount of movement needed to move Character
//
//  Returns:
//    None
//*************************************************************************************************

void ABombGameMode::MoveRightP1(float Value)
{
	// Get ABatteryCollectorCharacter for player 1
	ABombCharacter* myC = Cast<ABombCharacter>(UGameplayStatics::GetPlayerPawn(this, 1));
	if (myC != NULL)
	{
		myC->MoveRight(Value);
	}
}


//*************************************************************************************************
//## void PlantBombP0()
//
//  Description:
//    Try and place bomb for player 0
//
//  Parameters:
//    None:
//
//  Returns:
//    None
//*************************************************************************************************

void ABombGameMode::PlantBombP0()
{
	// Get ABatteryCollectorCharacter for player 0
	ABombCharacter* myC = Cast<ABombCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	PlantBomb(0,myC);
}



//*************************************************************************************************
//## void PlantBombP1()
//
//  Description:
//    Try and place bomb for player 1
//
//  Parameters:
//    None:
//
//  Returns:
//    None
//*************************************************************************************************

void ABombGameMode::PlantBombP1()
{
	// Get ABatteryCollectorCharacter for player 1
	ABombCharacter* myC = Cast<ABombCharacter>(UGameplayStatics::GetPlayerPawn(this, 1));
	PlantBomb(1, myC);
}



void ABombGameMode::PlantBomb(int32 playerIndex, class ABombCharacter* myC)
{
	if (myC != NULL)
	{

		TActorIterator< ABombHandler > ActorItr =
			TActorIterator< ABombHandler >(GetWorld());
		while (ActorItr)
		{
			//UE_LOG(LogTemp, Display, TEXT("%s"),*ActorItr->GetClass()->GetName());
			//UE_LOG(LogTemp, Display, TEXT("%s"),*ActorItr->GetClass()->GetDesc());
			//UE_LOG(LogTemp, Display, TEXT("%s"),*ActorItr->GetActorLabel());
			//UE_LOG(LogTemp, Display, TEXT("%s"),*ActorItr->GetClass()->GetDescription());
			if (ActorItr->GetActorLabel() == TEXT("LevelBombHandler"))
			{
				ActorItr->PlantBomb(myC->GetActorLocation(), playerIndex);
				break;
			}
			//next actor
			++ActorItr;
		}
	}
}




